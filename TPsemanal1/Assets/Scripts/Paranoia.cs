﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Paranoia : MonoBehaviour
{
    public Light LuzParanoia;
    public bool LuzApagada = false;

    public float cantParanoia = 0;

    public Linterna lint;


    void Update()
    {
        cantParanoia = Mathf.Clamp(cantParanoia, 0, 100);
        int valorBateria = (int)cantParanoia;
        
        if(lint.locura == true)
        {
            LuzApagada = true;
        }
        else
        {
            LuzApagada = false;
        }

 
        if (LuzApagada == true && cantParanoia >= 0)
        {
            cantParanoia += 0.9f * Time.deltaTime;
        }
        if (cantParanoia == 0)
        {
            LuzParanoia.intensity = 0f;
            
        }
        if (cantParanoia > 0 && cantParanoia <= 25)
        {
            LuzParanoia.intensity = 0.6f;
          
        }
        if (cantParanoia > 25 && cantParanoia <= 50)
        {
            LuzParanoia.intensity = 2f;
           
        }
        if (cantParanoia > 50 && cantParanoia <= 75)
        {
            LuzParanoia.intensity = 5f;
           
        }
        if (cantParanoia > 75 && cantParanoia <= 100)
        {
            LuzParanoia.intensity = 10f;
            

        }
        if(cantParanoia >= 100)
        {
            LuzParanoia.intensity = 100f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }
}