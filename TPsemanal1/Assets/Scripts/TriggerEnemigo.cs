﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnemigo : MonoBehaviour
{
    public GameObject Enemigo;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Jugador")
        {
            Enemigo.SetActive(true);
        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
