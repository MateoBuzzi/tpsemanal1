﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoFantasma : MonoBehaviour
{
    public GameObject Fantasma;
    public GameObject Jugador;
    public int Rapidez;

    void Start()
    {
        Jugador = GameObject.Find("Jugador");
    }

    private void Update()
    {
        transform.LookAt(Jugador.transform);
        transform.Translate(Rapidez * Vector3.forward * Time.deltaTime);
    }

}
