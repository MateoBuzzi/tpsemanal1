﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinternaPick : MonoBehaviour
{
    public GameObject DLinterna;
    public GameObject VisualPilas;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Jugador")
        {
            DLinterna.SetActive(true);
            DLinterna.GetComponent<Linterna>().Linternaenmano = true;
            VisualPilas.SetActive(true);
            Destroy(gameObject);
        }
    }
    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
