﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnemigoF : MonoBehaviour
{
    public GameObject Enemigo;
    public Paranoia para;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Jugador")
        {
            Enemigo.SetActive(false);
            para.cantParanoia += 0.5f * Time.deltaTime;
        }
    }


}
