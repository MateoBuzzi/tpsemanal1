﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Linterna : MonoBehaviour
{
    public Light LuzLinterna;
    public bool ActivLuz;
    public bool Linternaenmano;
    public float cantBateria = 50;
    public bool locura = false;
  

    [Header("Visuales")]
    public Image pila1;
    public Image pila2;
    public Image pila3;
    public Image pila4;
    public Sprite pilaVacia;
    public Sprite pilaLlena;
    public Text porcentaje;

    void Update()
    {
        cantBateria = Mathf.Clamp(cantBateria, 0, 100);
        int valorBateria = (int)cantBateria;
        porcentaje.text = valorBateria.ToString() + "%";

        if(Input.GetKeyDown(KeyCode.F) && Linternaenmano == true)
        {
            ActivLuz = !ActivLuz;
            if(ActivLuz == true)
            {
                locura = false;
                LuzLinterna.enabled = true;
            }
            if(ActivLuz == false)
            {
                locura = true;
                LuzLinterna.enabled = false;
            }
        }
        if(ActivLuz == true && cantBateria > 0)
        {
            cantBateria -= 10f * Time.deltaTime;
        }
        if(cantBateria == 0)
        {
            LuzLinterna.intensity = 0f;   
            pila1.sprite = pilaVacia;
            locura = true;
        }
        if(cantBateria > 0 && cantBateria <= 25)
        {
            LuzLinterna.intensity = 15f;
            pila1.sprite = pilaLlena;
            pila2.sprite = pilaVacia;
        }
        if(cantBateria > 25 && cantBateria <= 50)
        {
            LuzLinterna.intensity = 25f;
            pila2.sprite = pilaLlena;
            pila3.sprite = pilaVacia;
        }
        if (cantBateria > 50 && cantBateria <= 75)
        {
            LuzLinterna.intensity = 35f;
            pila3.sprite = pilaLlena;
            pila4.sprite = pilaVacia;
        }
        if (cantBateria > 75 && cantBateria <= 100)
        {
            LuzLinterna.intensity = 45f;
            pila4.sprite = pilaLlena;
          
        }

    }
}
